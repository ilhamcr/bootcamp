<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'HomeController@Home');
Route::get('/form', 'AuthController@Register');
Route::post('/welcome', 'AuthController@Welcome');

Route::get('/data-table', function(){
    return view('page.data-table');
});
Route::get('/table', function(){
    return view('page.table');
});

Route::group(['middleware' => ['auth']], function () {
//CRUD cast
//create
Route::get('/cast/create', 'CastController@create');
Route::post('/cast', 'CastController@store');
//read
Route::get('/cast', 'CastController@index');
Route::get('/cast/{cast_id}', 'CastController@show');
//update
Route::get('/cast/{cast_id}/edit', 'CastController@edit');
Route::put('/cast/{cast_id}/', 'CastController@update');
//delete
Route::delete('/cast/{cast_id}', 'CastController@destroy');

//CRUD genre
//Route::resource('genre', 'GenreController');
Route::get('/genre/create', 'GenreController@create');
Route::post('/genre', 'GenreController@store');
//read
Route::get('/genre', 'GenreController@index');
Route::get('/genre/{genre_id}', 'GenreController@show');
//update
Route::get('/genre/{genre_id}/edit', 'GenreController@edit');
Route::put('/genre/{genre_id}/', 'GenreController@update');
//delete
Route::delete('/genre/{genre_id}', 'GenreController@destroy');

//Create Kritik
Route::resource('kritik', 'KritikController');

//Profil
Route::resource('profile','ProfilController')->only([
    'index','update'
    ]);
Route::resource('kritik', 'KritikController')->only(['store']);
Route::get('/home', 'HomeController@index')->name('home');
});

Auth::routes();

//CRUD Film
Route::resource('film', 'FilmController');

Route::group(['prefix' => 'laravel-filemanager', 'middleware' => ['web', 'auth']], function () {
    \UniSharp\LaravelFilemanager\Lfm::routes();
});

