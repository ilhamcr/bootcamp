<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use RealRashid\SweetAlert\Facades\Alert;

class CastController extends Controller
{
    public function create(){
        return view('cast.create');
    }

    public function store(Request $request){
        //dd($request->all());
        $request->validate([
            'nama' => 'required|min:5',
            'umur' => 'required',
            'bio' => 'required',
        ],
        [
            'nama.required' => 'Nama harus diisi',
            'nama.min' => 'Nama harus lebih dari 5 karakter',
            'umur.required' => 'Umur harus diisi',
            'bio.required' => 'Biodata harus diisi'
        ]
    );

        DB::table('cast')->insert([
            'nama' => $request['nama'],
            'umur' => $request['umur'],
            'bio' => $request['bio']
        ]);
        Alert::success('Berhasil', 'Cast berhasil ditambahkan');    
        return redirect('/cast');
    }

    public function index(){
        $cast = DB::table('cast')->get();
        return view('cast.index', compact('cast'));
    }

    public function show($id){
        $cast = DB::table('cast')->where('id', $id)->first();
        return view('cast.show', compact('cast'));
    }

    public function edit($id){
        $cast = DB::table('cast')->where('id', $id)->first();
        return view('cast.edit', compact('cast'));
    }

    public function update($id , Request $request){
        $request->validate([
            'nama' => 'required|min:5',
            'umur' => 'required',
            'bio' => 'required',
        ],
        [
            'nama.required' => 'Nama harus diisi',
            'nama.min' => 'Nama harus lebih dari 5 karakter',
            'umur.required' => 'Umur harus diisi',
            'bio.required' => 'Biodata harus diisi'
        ]
    );
        DB::table('cast')
            ->where('id', $id)
            ->update([
                'nama' => $request['nama'],
                'umur' => $request['umur'],
                'bio' => $request['bio']
        ]);
        Alert::success('Berhasil', 'Cast berhasil diubah');
        return redirect('/cast');
    }

    public function destroy($id){
        DB::table('cast')->where('id', $id)->delete();
        return redirect('/cast');
    }
}
