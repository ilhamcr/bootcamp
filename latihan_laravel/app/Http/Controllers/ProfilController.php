<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Profile;

class ProfilController extends Controller
{
    public function index(){
        $profile = Profile::where('users_id', Auth::id())->first();
        //dd($profile);
        return view('profile.edit', compact('profile'));
    }
    public function update($id, Request $request){
        $request->validate([
            'umur' => 'required',
            'bio' => 'required',
            'alamat' => 'required',
        ],
        [
            'umur.required' => 'Umur harus diisi',
            'bio.required' => 'Biodata harus diisi',
            'alamat.required' => 'Alamat harus diisi'
        ]);

        $profile = Profile::find($id);
 
        $profile->umur = $request['umur'];
        $profile->bio = $request['bio'];
        $profile->alamat = $request['alamat'];
 
        $profile->save();
        return redirect('/profile');
    }
}
