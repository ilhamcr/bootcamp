<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Kritik extends Model
{
    protected $table = 'kritik';

    protected $fillable = ['users_id','film_id','content','point'];

    public function users(){
        return $this->belongsTo('App\User', 'users_id');
    }

    public function film(){
        return $this->belongsTo('App\Film', 'film_id');
    }
}
