<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Profile extends Model
{
    protected $table = 'profile';

    protected $fillable = ['users_id','umur','bio','alamat'];

    public function user()
    {
        return $this->belongsTo('App\User','users_id');
    }
}

