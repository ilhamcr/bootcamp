@extends('layout.master')
@section('title')
Genre
@endsection
    
@section('content')
<a href="/genre/create" class="btn btn-primary mb-3">Tambah Genre</a>

<table class="table">
  <thead class="thead-dark">
    <tr>
      <th scope="col">No</th>
      <th scope="col">Nama</th>
      <th scope="col">Action</th>
    </tr>
  </thead>
  <tbody>
    @forelse ($genre as $key => $item)
        <tr>
        <th scope="row">{{$key+1}}</th>
        <td>{{$item->nama}}</td>
        <td>
            <form action="/genre/{{$item->id}}" method="post"> 
            <a href="/genre/{{$item->id}}/edit" class="btn btn-warning btn-sm">Edit</a>
            <a href="/genre/{{$item->id}}/" class="btn btn-info btn-sm">Detail</a>
              @csrf
              @method('delete')
              <input type="submit" class="btn btn-danger btn-sm" value="delete">
            </form>
        </td>
        </tr>
    @empty
        <tr>
            <td>Data genre masih kosong</td>
        </tr>
    @endforelse    
  </tbody>
</table>
@endsection