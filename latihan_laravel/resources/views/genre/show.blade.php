@extends('layout.master')
@section('title')
Detail Genre
@endsection
    
@section('content')
    <h1>{{$genre->nama}}</h1>
    <div class="row">
        @forelse ($genre->film as $item)
        <div class="col-4">
            <div class="card" style>
                <img class="card-img-top" src="{{asset('image/'.$item->poster)}}" width="100%" height="200px" alt="Card image cap">
                <div class="card-body">
                    <h4 class="card-title"> {{$item->judul}}</h4>
                    <p class="card-text">{{$item->ringkasan}}</p>
                    <a href="/film/{{$item->id}}" class="btn btn-primary btn-sm">Detail</a>
                </div>
            </div>
        </div>
     @empty
       <h1>Tidak ada genre</h1>
     @endforelse
    </div>


@endsection