@extends('layout.master')
@section('title')
Buat Data Genre
@endsection
    
@section('content')
<form method="POST" action="/genre">
    @csrf
  <div class="form-group">
    <label>Nama</label>
    <input type="text" class="form-control" name="nama" id="nama">
  </div>
    @error('nama')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    <button type="submit" class="btn btn-primary">Submit</button>
</form>
@endsection