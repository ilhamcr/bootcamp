@extends('layout.master')
@section('title')
Buat Account Baru
@endsection

@section('content')
    <h3>Sign Up Form</h3>
    <form action="/welcome" method="post">
        @csrf
        <label for="">First Name:</label><br><br>
        <input type="text" name="fname"><br><br>
        <label for="">Last Name:</label><br><br>
        <input type="text" name="lname"><br><br>
        <label for="">Gender:</label><br><br>
        <input type="radio">Male<br>
        <input type="radio">Female<br>   
        <input type="radio">Other<br><br>
        <label for="">Nationality:</label><br><br>
        <select name="N">
            <option value="Indonesia">Indonesia</option>
            <option value="Singapore">Singapore</option>
            <option value="Malaysia">Malaysia</option>
            <option value="Philipines">Philipines</option>
        </select><br><br>
        <label for="">Language Spoken:</label><br><br>
        <input type="checkbox">Indonesia<br>
        <input type="checkbox">English<br>   
        <input type="checkbox">Other<br><br>
        <label for="">Bio:</label><br><br>
        <textarea name="bio" id="" cols="30" rows="10"></textarea><br>
        <input type="submit" value="Sign Up">
    </form>
@endsection