@extends('layout.master')
@section('title')
Cast
@endsection
    
@section('content')
<a href="/cast/create" class="btn btn-primary mb-3">Tambah Cast</a>

<table class="table">
  <thead class="thead-dark">
    <tr>
      <th scope="col">No</th>
      <th scope="col">Nama</th>
      <th scope="col">Umur</th>
      <th scope="col">Biodata</th>
      <th scope="col">Action</th>
    </tr>
  </thead>
  <tbody>
    @forelse ($cast as $key => $item)
        <tr>
        <th scope="row">{{$key+1}}</th>
        <td>{{$item->nama}}</td>
        <td>{{$item->umur}}</td>
        <td>{!!$item->bio!!}</td>
        <td>
            <form action="/cast/{{$item->id}}" method="post"> 
            <a href="/cast/{{$item->id}}" class="btn btn-info btn-sm">Detail</a>
            <a href="/cast/{{$item->id}}/edit" class="btn btn-warning btn-sm">Edit</a>
              @csrf
              @method('delete')
              <input type="submit" class="btn btn-danger btn-sm" value="delete">
            </form>
        </td>
        </tr>
    @empty
        <tr>
            <td>Data cast masih kosong</td>
        </tr>
    @endforelse    
  </tbody>
</table>
@endsection