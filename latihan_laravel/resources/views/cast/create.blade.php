@extends('layout.master')
@section('title')
Buat Data Cast
@endsection
    
@section('content')
<form method="POST" action="/cast">
    @csrf
  <div class="form-group">
    <label>Nama</label>
    <input type="text" class="form-control" name="nama">
  </div>
    @error('nama')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror
  <div class="form-group">
    <label>Umur</label>
    <input type="text" class="form-control" name="umur">
  </div>
    @error('umur')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror
  <div class="form-group">
    <label>Biodata</label>
    <textarea class="form-control" id="exampleFormControlTextarea1" rows="3" name="bio"></textarea>
  </div>
    @error('bio')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror
  <button type="submit" class="btn btn-primary">Submit</button>
</form>
@push('script')
  <script src="https://cdn.tiny.cloud/1/z278ironqdaf3hee4badge3qyivyehuuiks2u9yue65at8ru/tinymce/6/tinymce.min.js" referrerpolicy="origin"></script>
  <script>
    tinymce.init({
      selector: 'textarea',
      plugins: 'a11ychecker advcode casechange export formatpainter image editimage linkchecker autolink lists checklist media mediaembed pageembed permanentpen powerpaste table advtable tableofcontents tinycomments tinymcespellchecker',
      toolbar: 'a11ycheck addcomment showcomments casechange checklist code export formatpainter image editimage pageembed permanentpen table tableofcontents',
      toolbar_mode: 'floating',
      tinycomments_mode: 'embedded',
      tinycomments_author: 'Author name',
    });
  </script>
@endpush
@endsection