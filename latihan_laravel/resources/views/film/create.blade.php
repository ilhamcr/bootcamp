@extends('layout.master')
@section('title')
Buat Data Film
@endsection
    
@section('content')
<form method="POST" action="/film" enctype="multipart/form-data">
    @csrf
  <div class="form-group">
    <label>Judul</label>
    <input type="text" class="form-control" name="judul">
  </div>
    @error('judul')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    <div class="form-group">
    <label>Ringkasan</label>
    <textarea class="form-control" id="exampleFormControlTextarea1" rows="3" name="ringkasan"></textarea>
  </div>
    @error('ringkasan')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror
  <div class="form-group">
    <label>Tahun</label>
    <input type="text" class="form-control" name="tahun">
  </div>
    @error('tahun')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror
  <div class="form-group">
    <label>Genre</label>
    <select name="genre_id" class="form-control">
        <option value="">--Pilih Genre--</option>
        @foreach($genre as $item)
            <option value="{{$item->id}}">{{$item->nama}}</option>
        @endforeach
    </select>    
  </div>
  <div class="form-group">
    <label>Poster</label>
    <input type="file" class="form-control" name="poster">
  </div>
    @error('poster')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror
  <button type="submit" class="btn btn-primary">Submit</button>
</form>
@push('script')
  <script src="https://cdn.tiny.cloud/1/z278ironqdaf3hee4badge3qyivyehuuiks2u9yue65at8ru/tinymce/6/tinymce.min.js" referrerpolicy="origin"></script>
  <script>
    tinymce.init({
      selector: 'textarea',
      plugins: 'a11ychecker advcode casechange export formatpainter image editimage linkchecker autolink lists checklist media mediaembed pageembed permanentpen powerpaste table advtable tableofcontents tinycomments tinymcespellchecker',
      toolbar: 'a11ycheck addcomment showcomments casechange checklist code export formatpainter image editimage pageembed permanentpen table tableofcontents',
      toolbar_mode: 'floating',
      tinycomments_mode: 'embedded',
      tinycomments_author: 'Author name',
    });
  </script>
@endpush

@endsection