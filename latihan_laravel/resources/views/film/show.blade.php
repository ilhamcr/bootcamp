@extends('layout.master')

@push('script-head')
<script src="/path-to-your-tinymce/tinymce.min.js"></script>

@endpush

@section('title')
Film
@endsection

@section('content')
<div class="row">
    <div class="col"> 
        <div class="card" style>
            <img class="card-img-top" src="{{asset('image/'.$film->poster)}}" alt="Card image cap">
            <div class="card-body">
                <h5 class="card-title">{{$film->judul}}</h5>
                <p class="card-text">{{$film->ringkasan}}</p>
                <a href="/film" class="btn btn-primary">Kembali</a>
            </div>
        </div>
    </div>
</div>


@auth

<h4>Nilai Film</h4>

@forelse($film->kritik as $item)
    <div class="card">
      <div class="card-header">
        {{$item->users->name}}
      </div>
      <div class="card-body">
        <p class="card-text">{{$item->content}}</p>
      </div>
    </div>
@empty

@endforelse
    <form action="/kritik" method="post">
        @csrf
        <input type="hidden" value="{{$film->id}}" name="film_id">
        <textarea name="content" placeholder="Komentar" class="form-control my-editor">{!! old('content', $content ?? '') !!}</textarea>
        <label>Point</label>
        <input type="number" class="form-control" name="point">
        <input type="submit" value="Nilai" class="btn btn-primary mt-2">
    </form>
@endauth



@push('script-footer')
<script>
  var editor_config = {
    path_absolute : "/",
    selector: 'textarea.my-editor',
    relative_urls: false,
    plugins: [
      "advlist autolink lists link image charmap print preview hr anchor pagebreak",
      "searchreplace wordcount visualblocks visualchars code fullscreen",
      "insertdatetime media nonbreaking save table directionality",
      "emoticons template paste textpattern"
    ],
    toolbar: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image media",
    file_picker_callback : function(callback, value, meta) {
      var x = window.innerWidth || document.documentElement.clientWidth || document.getElementsByTagName('body')[0].clientWidth;
      var y = window.innerHeight|| document.documentElement.clientHeight|| document.getElementsByTagName('body')[0].clientHeight;

      var cmsURL = editor_config.path_absolute + 'laravel-filemanager?editor=' + meta.fieldname;
      if (meta.filetype == 'image') {
        cmsURL = cmsURL + "&type=Images";
      } else {
        cmsURL = cmsURL + "&type=Files";
      }

      tinyMCE.activeEditor.windowManager.openUrl({
        url : cmsURL,
        title : 'Filemanager',
        width : x * 0.8,
        height : y * 0.8,
        resizable : "yes",
        close_previous : "no",
        onMessage: (api, message) => {
          callback(message.content);
        }
      });
    }
  };

  tinymce.init(editor_config);
</script>

@endpush
@endsection