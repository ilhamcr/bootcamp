@extends('layout.master')
@section('title')
Film
@endsection
    
@section('content')
@auth
<a href="/film/create" class="btn btn-primary mb-3">Tambah Film</a>
@endauth
<div class="row">
@forelse ($film as $item)
    <div class="col-4"> 
        <div class="card" style>
            <img class="card-img-top" src="{{asset('image/'.$item->poster)}}" width="100%" height="200px" alt="Card image cap">
            <div class="card-body">
                <h1 class="card-title">{{$item->judul}}</h1>
                <span class= "badge badge-primary m-1">{{$item->genre->nama}}</span>
                <p class="card-text">{!!$item->ringkasan!!}</p>
                @auth
                <form action="/film/{{$item->id}}" method="post">
                @csrf
                @method('delete')
                    <a href="/film/{{$item->id}}" class="btn btn-primary btn-sm">Detail</a>
                    <a href="/film/{{$item->id}}/edit" class="btn btn-warning btn-sm">edit</a>
                    <input type="submit" class="btn btn-danger btn-sm" value="delete">
                </form>
                @endauth
                @guest
                    <a href="/film/{{$item->id}}" class="btn btn-primary btn-sm">Detail</a>
                @endguest
            </div>
        </div>
    </div>
@empty
    <h1>Tidak ada film</h1>    
@endforelse
</div>
@endsection